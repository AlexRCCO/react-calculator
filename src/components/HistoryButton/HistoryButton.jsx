import React from 'react';
import './HistoryButton.css';

export const HistoryButton = (props) => (
    <div className="history-btn" onClick={props.handleHistory}>History</div>
)