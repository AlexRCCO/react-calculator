import React from 'react';
import './InputHistory.css';

export const InputHistory = (props) => (
    <div className="history-input">{props.historys.map((history,key) => (<p className="history-operator" key={key}>{history}</p>))}</div>
)