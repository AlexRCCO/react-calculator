import React, { Component } from 'react';
import './App.css';
import { Input } from '../Input/Input.jsx';
import { Button } from '../Button/Button.jsx';
import * as math from 'mathjs'
import { ClearButton } from '../ClearButton/ClearButton';
import { HistoryButton } from '../HistoryButton/HistoryButton';
import { InputHistory } from '../InputHistory/InputHistory';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      input: '',
      historys: []
    }
    this.addCalculate = this.addCalculate.bind(this);
  };

  addCalculate = (value) => {
    this.setState({input: this.state.input + value})
  }

  handleEqual = () => {
    this.setState({input: math.eval(this.state.input)})
    this.setState({historys:this.state.historys.concat([this.state.input + '=' + math.eval(this.state.input)])})
  }

  render() {
    return (
      <div className="App">
        <div className="calc-wrapper">
          <div className="input-wrapper">
            <Input input={this.state.input}></Input>
            <InputHistory historys={this.state.historys}></InputHistory>
          </div>
            <div className="row">
              <Button handleClick={this.addCalculate}>7</Button>
              <Button handleClick={this.addCalculate}>8</Button>
              <Button handleClick={this.addCalculate}>9</Button>
              <Button handleClick={this.addCalculate}>/</Button>
            </div>
            <div className="row">
              <Button handleClick={this.addCalculate}>4</Button>
              <Button handleClick={this.addCalculate}>5</Button>
              <Button handleClick={this.addCalculate}>6</Button>
              <Button handleClick={this.addCalculate}>*</Button>
            </div>
            <div className="row">
              <Button handleClick={this.addCalculate}>1</Button>
              <Button handleClick={this.addCalculate}>2</Button>
              <Button handleClick={this.addCalculate}>3</Button>
              <Button handleClick={this.addCalculate}>+</Button>
            </div>
            <div className="row">
              <Button handleClick={this.addCalculate}>.</Button>
              <Button handleClick={this.addCalculate}>0</Button>
              <Button handleClick={this.handleEqual}>=</Button>
              <Button handleClick={this.addCalculate}>-</Button>
            </div>
            <div className="clear">
              <HistoryButton handleHistory={() => this.setState({historys: [this.state.historys.concat([this.state.input])] })}>History</HistoryButton>
              <ClearButton handleClear={() => this.setState({input: '', historys : []})}>Clear</ClearButton>
            </div>
        </div>
      </div>
    );
  }
}

export default App;
