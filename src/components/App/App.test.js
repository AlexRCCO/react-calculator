import React from 'react';
import { shallow, mount, render } from 'enzyme';
import renderer from 'react-test-renderer';
import {Input} from '../Input/Input';
import {InputHistory} from '../InputHistory/InputHistory';
import { ClearButton } from '../ClearButton/ClearButton';
import {Button} from '../Button/Button';

it('render correctly input component', () => {
    const InputComponent = renderer.create(<Input/>).toJSON();
    expect(InputComponent).toMatchSnapshot();
})

// it('render correctly InputHistory component', () => {
//     const InputHistoryComponent = renderer.create(<InputHistory/>).toJSON();
//     expect(InputHistoryComponent).toMatchSnapshot();
// })

it('render correctly ClearButton', () => {
    const ClearButtonComponent = renderer.create(<ClearButton/>).toJSON();
    expect(ClearButtonComponent).toMatchSnapshot();
})

it('render correctly Button component', () => {
    const ButtonComponent = renderer.create(<Button/>).toJSON();
    expect(ButtonComponent).toMatchSnapshot();
})