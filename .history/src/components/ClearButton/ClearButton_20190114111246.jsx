import React from 'react';
import './ClearButton.css';

export const ClearButton = (props) => (
    <div className="vlear-btn" onClick={props.handleClear}>Clear</div>
) 