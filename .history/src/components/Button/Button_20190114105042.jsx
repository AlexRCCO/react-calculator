import React, {Component} from 'react';
import './Button.css';

const isOperator = val => {
    return !isNaN(val) || val === "." || val ==="=";
}

export const Button = props => <div className={`button-wrapper ${
    isOperator(props.children) ? null : "operator"
}`} onClick={() => props.handleClick(props.children)}>
{props.children}</div>


// class Button extends Component {
//     constructor(props) {
//     super(props);

//     this.state = {
//         value: 0
//     }

// }

// numberValue(e){
//    this.setState({ value: e.target.value })
//    console.log(this.state.value)
// }   

//     render() {   
//         const numbers = [1,2,3,4,5,6,7,8,9]
//         const opérations = ['%', '*', '+', '-']
//         const listNumber = numbers.map((number, index) => <button value={this.state.value} id={index} onClick={(value) => this.numberValue(value) }>{number}</button>)
//         const listOpérations = opérations.map((opération) => <button>{opération}</button>)
        
//         return(
//             <div className="calculette">
//                 <div className="button">{listNumber}</div>
//                 <div className="opérateur">{listOpérations}</div>
//             </div>
//             )
//         }
// }

